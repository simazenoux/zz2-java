package fr.isima.vehicule;

public abstract class Vehicule {
    
    protected String immat;
    
    public Vehicule(String immat)
    {
        this.immat = immat;
    }
    
    abstract public void afficher();
    
    public void avancer()
    {
        System.out.println("J'avance");
    }

}
