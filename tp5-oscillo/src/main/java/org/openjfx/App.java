package org.openjfx;


import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {

        
        
        var root = new HBox();
        var scene = new Scene(root, 640, 480);
        

        int buttonWidth = 120;

        
        Canvas canvas = new Canvas(scene.getWidth()-buttonWidth,scene.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
 

        generateOscillo(canvas, gc);

        
        root.getChildren().add(canvas);

        Button button = new Button("Rafraichir");
        button.setPrefHeight(scene.getHeight());
        button.setPrefWidth(buttonWidth);
        button.setOnAction(new javafx.event.EventHandler<ActionEvent>() {
                @Override 
                public void handle(ActionEvent e){
                    gc.clearRect(0, 0, root.getWidth(), root.getHeight());
                    generateOscillo(canvas, gc);
                }
            }
        );
        root.getChildren().add(button);
        
        stage.setScene(scene);
        stage.setTitle("Oscilloscope");
        stage.show();
    }

    private static void generateOscillo(Canvas canvas, GraphicsContext gc){
        Random random = new Random();
        double previousY = canvas.getHeight()/2;
        for (int i=0; i< canvas.getWidth()-1; i++)
        {
            double y = random.nextDouble() * canvas.getHeight();
            gc.strokeLine(i, previousY, i+1, y);
            previousY = y;
        }
        
    }

    public static void main(String[] args) {
        launch();
    }

}