package fr.isima.vehicule;

import java.util.Random;

public class Voiture extends Vehicule {
    private String couleur;
    private int places;

    public Voiture(String immat, String couleur, int places)
    {
        super(immat);
        this.couleur = couleur;
        this.places = places;
    }
    

    public Voiture(int places)
    {
        super("NULL");
        Random r = new Random();

        this.immat = "ZZ"+r.nextInt(1000)+"ZZ";
        
        if (r.nextBoolean())
        {
            this.couleur = "rouge";
        }
        else
        {
            this.couleur = "bleu";
        }
        this.places = places;
    }

    @Override
    public void avancer()
    {
        System.out.println("Voiture go WROOOOM");
    }

    public void afficher()
    {
        System.out.println("Je suis une voiture avec " + places + " places de couleur " + couleur + ". Ma plaque est " + immat);
    }
}
