package fr.isima;

import fr.isima.vehicule.*;

/**
 * Hello world!
 *
 */
public class Societe 
{
    public static void main( String[] args )
    {
        Camion camion = new Camion("AB123CD", 32);
        Voiture voiture = new Voiture(5);

        camion.afficher();
        camion.avancer();

        voiture.afficher();
        voiture.avancer();
    }
}
