package org.openjfx;

import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.paint.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {

        var pane = new Pane();
        var root = new StackPane(pane);
        var scene = new Scene(root, 640, 480);
        
        // var canvas = new Canvas(scene.getWidth(),scene.getHeight());
        // GraphicsContext gc = canvas.getGraphicsContext2D();
        
        generatePicasso(pane,scene);
        

        // root.getChildren().add(pane);

        stage.setScene(scene);
        stage.setTitle("Picasso");
        stage.show();
    }

    private static void generatePicasso(Pane pane, Scene scene)
    {
        Random random = new Random();
        for (int i=0; i<1000; i++)
        {
            var rectangle = new Rectangle();
            rectangle.setWidth(random.nextInt(100) + 20.0);
            rectangle.setHeight(random.nextInt(100) + 20.0);
            rectangle.setX(random.nextInt((int) scene.getWidth())-50);
            rectangle.setY(random.nextInt((int) scene.getHeight())-50);
            Color color = new Color(random.nextDouble(),random.nextDouble(),random.nextDouble(),random.nextDouble());
            rectangle.setFill(color);
    
            pane.getChildren().add(rectangle);
        }
    }

    public static void main(String[] args) {
        launch();
    }

}