package fr.isima.vehicule;

public class Camion extends Vehicule 
{
    private float capacite;

    public Camion(String immat, float capacite)
    {
        super(immat);
        this.capacite = capacite;
    }

    public void afficher()
    {
        System.out.println("Je suis un camion avec une capacité de chargement de " + capacite + "tonnes");
    }
}
