package org.openjfx;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {


    @Override
    public void start(Stage stage) {
        var javaVersion = SystemInfo.javaVersion();
        var javafxVersion = SystemInfo.javafxVersion();

        var btn = new Button();
        btn.setText("CLIQUE");

        
        var label = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        // var root = new StackPane(label, btn);
        var root = new BorderPane();
        var root1 = new HBox();

        for(int i=0; i<10; i++)
        {
            root1.getChildren().add(new ButtonWhichIncrementOnClick());
        }

        root.setBottom(new Button("Bas"));
        root.setTop(root1);
        root.setLeft(new Button("Left"));
        root.setRight(new Button("Right"));
        root.setCenter(label);
        var scene = new Scene(root, 640, 480);
        stage.setScene(scene);
        stage.setTitle("Mon application");
        // btn.addEventHandler(, arg1);

        


        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}