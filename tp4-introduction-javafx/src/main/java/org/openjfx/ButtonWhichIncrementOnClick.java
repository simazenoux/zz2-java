package org.openjfx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ButtonWhichIncrementOnClick extends Button implements EventHandler<ActionEvent> {
    
    private int cpt;
    
    public ButtonWhichIncrementOnClick()
    {
        super("0");
        cpt = 0;
        setOnAction(this);
    }

    public void incrementer() {
        ++cpt;
        setText(String.valueOf(cpt));
    } 

    @Override
    public void handle(ActionEvent e)
    {
        incrementer();
    }
     
}

